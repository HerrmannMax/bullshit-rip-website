<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Yes!" />
        <meta name="author" content="Prof. Dr. P.Zimmermann und Dr. M.Herrmann" />
        <title>Klinische Plazebo RIP Studie</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css2?family=Tinos:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&amp;display=swap" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <video class="bg-video" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop"><source src="assets/mp4/bg.mp4" type="video/mp4" /></video>
        <div class="masthead">
            <div class="masthead-content text-white">
                <div class="container-fluid px-4 px-lg-0">
                    <h1 class="fst-italic lh-1 mb-4">Wilkommen bei der RIP im Park Plazebo Studie</h1>
		    Name: <?php echo $_GET["name"]; ?><br>
                    Geburtsdatum: <?php echo $_GET["bdate"]; ?><br>
                    Teilnehmer ID: <?php echo $_GET["acode"]; ?><br>
		    <br>
                    Ihre Eingabe war erfolgreich!<br>
                    Wir haben auf Basis eines KI-Algorythmus einen persönliche ID generiert<br>
		    Wichtigeste Kriterien der ID:<br>
                    - <strong>nicht zurückverfolgbar</strong><br>
		    - Datenschutzkonform<br>
                    - absolut einmalig<br>
                    Teilnehmer ID: <strong><?php echo $_GET["name"] . "-" . $_GET["bdate"] . "-". $_GET["acode"];?></strong><br>
		    Ihr persönlicher Link:<br>
		    <?php $TidName = str_replace(' ', '_', $_GET["name"]); ?>
                    <a href="umfrage.php?<?php echo "tid=" . $TidName . "-" . $_GET["bdate"] . "-". $_GET["acode"];?>">httpextremsicher://studie.rip.gov.ru/umfrage?<?php echo "tid=" . $_GET["name"] . "-" . $_GET["bdate"] . "-". $_GET["acode"];?></a> <br>
		    <br>
		    Vielen Danke!!!!!!111!11!1elf<br>
		    <br>
                    <br>
		    <a href="datenschutz.html">Weitere Informationen zu "Datenschutz"</a>

                </div>
            </div>
        </div>
        <div class="social-icons">
            <div class="d-flex flex-row flex-lg-column justify-content-center align-items-center h-100 mt-3 mt-lg-0">
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-twitter"></i></a>
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-instagram"></i></a>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
