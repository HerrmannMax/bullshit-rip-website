<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Yes!" />
        <meta name="author" content="Prof. Dr. P.Zimmermann und Dr. M.Herrmann" />
        <title>Klinische Plazebo RIP Studie</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css2?family=Tinos:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&amp;display=swap" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
	<script type="module" src="https://unpkg.com/friendly-challenge@0.9.2/widget.module.min.js" async defer></script>
	<script nomodule src="https://unpkg.com/friendly-challenge@0.9.2/widget.min.js" async defer></script>
    </head>
    <body>
        <video class="bg-video" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop"><source src="assets/mp4/bg.mp4" type="video/mp4" /></video>
        <div class="masthead">
            <div class="masthead-content text-white">
                <div class="container-fluid px-4 px-lg-0">
                    <h1 class="fst-italic lh-1 mb-4">Wilkommen bei der RIP im Park Plazebo Studie</h1>
                    <p class="mb-5">Bitte geben Sie hier Name, Geburtsdatum und Access-Cod3 ein, um fortzufahren</p>
                    <!-- to get an API token!-->
		    <form action="generate_id.php" method="get">
		    Name: <input type="text" name="name" required /><br>
		    Geburtsdatum: <input type="text" name="bdate" required /><br>
		    Access-Code: <input type="text" name="acode" required /><br>
		    <br>
		    <div class="frc-captcha" data-sitekey="FCMKU4R3LE2BOT93">
                    </div>
		    <br>
		    <button class="btn btn-primary" id="submit" type="submit">Start</button>
		    </form>
		    </div>
		    <br>

		    <a href="datenschutz.html">Weitere Informationen zu "Datenschutz"</a>
                </div>
            </div>
        </div>
        <div class="social-icons">
            <div class="d-flex flex-row flex-lg-column justify-content-center align-items-center h-100 mt-3 mt-lg-0">
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-twitter"></i></a>
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-instagram"></i></a>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
