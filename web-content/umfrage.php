<!DOCTYPE html> <html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Yes!" />
        <meta name="author" content="Prof. Dr. P.Zimmermann und Dr. M.Herrmann" />
        <title>Klinische Plazebo RIP Studie</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css2?family=Tinos:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&amp;display=swap" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <video class="bg-video" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop"><source src="assets/mp4/bg.mp4" type="video/mp4" /></video>
        <div class="masthead">
            <div class="masthead-content text-white">
                <div class="container-fluid px-4 px-lg-0">
		    <h4>
		      Welcome unidetifizierbarer Teilnehmer mit abosult zufälligen ID:
    		      <br>
		    </h4>
		    <h3>
                      <?php echo $_GET["tid"]; ?>
		    </h3>
                    <br>
                    <br>
		    <fieldset>      
		      <legend>Findest du einen Kater haben nervig?</legend>      
		      <input type="checkbox" name="favorite_pet" value="Ja">Ja<br>      
		      <input type="checkbox" name="favorite_pet" value="Nein">Nein<br>      
		      <input type="checkbox" name="favorite_pet" value="Ich hab keine Katze">Ich hab keine Katze<br>      
		      <br>      
		    </fieldset>
		    <fieldset>      
		      <legend>Was machst du normalerweise gegen Kater?</legend>      
		      <input type="text" name="sample_text" value=""><br>
		      <br>      
		    </fieldset>
        <fieldset>      
		      <legend>Hast du Arnika mit auf dem Festival?</legend>      
		      <input type="checkbox" name="who_is_arnika" value="Ja">Ja<br>      
		      <input type="checkbox" name="who_is_arnika" value="Nein">Nein<br>      
		      <input type="checkbox" name="who_is_arnika" value="Ich kenne keine Annika">Ich kenne keine Annika<br>      
		      <br>      
		    </fieldset>
        <fieldset>      
		      <legend>Hast du vor in naher Zukunft jegliche Nahrung abzulehnen um dich ausschliesslich von Lichtenergie zu ernähren??</legend>      
		      <input type="checkbox" name="lightenergy" value="Ja">Ja<br>      
		      <input type="checkbox" name="lightenergy" value="Nein">Nein<br>      
		      <input type="checkbox" name="lightenergy" value="Hast du Lack gesoffen?">Hast du Lack gesoffen?<br>      
		      <br>      
		    </fieldset>
        <fieldset>      
		      <legend>Wirfst du immer alles ein, was dir Fremde auf dem Festival in die Hand drücken?</legend>      
		      <input type="checkbox" name="favorite_pet" value="Klar macht doch jeder">Klar macht doch jeder<br>      
		      <input type="checkbox" name="favorite_pet" value="Ich schmeis euern Scheis auch gleich weg wenn du nicht kuckst">Ich schmeis euern Scheis auch gleich weg wenn du nicht kuckst<br>      
		      <input type="checkbox" name="favorite_pet" value="Nur bei euch weil ihr so vertrauenswürdig und HOT seid">Nur bei euch weil ihr so vertrauenswürdig und HOT seid<br>      
		      <br>      
		    </fieldset>
        <fieldset>      
		      <legend>Halte dein Endgerät für 5 Sekunden in die Höhe, dass wir deine Energie testen können.</legend>      
		      <input type="checkbox" name="energie_testing" value="Erledigt">Erledigt!<br>     
		      <br>      
		    </fieldset>
        <legend>Erhöhe nun die Lautstärke deines Mobiltelefons und lass dir deiner Energie berechnen!</legend>  
      	<form action="rickroll.mp4">
          <input type="submit" value="Eingabe speichern und Energie berechnen!">
      	</form>
		    <br>
		    <a href="datenschutz.html">Weitere Informationen zu "Datenschutz"</a>
                </div>
            </div>
        </div>
        <div class="social-icons">
            <div class="d-flex flex-row flex-lg-column justify-content-center align-items-center h-100 mt-3 mt-lg-0">
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-twitter"></i></a>
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-dark m-3" href="#!"><i class="fab fa-instagram"></i></a>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
